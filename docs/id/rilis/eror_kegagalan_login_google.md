## Catatan
Dokumen asli: [google_login_failure_error.md](https://gitlab.com/wayne-inc/wayneos/-/blob/master/docs/en/release/google_login_failure_error.md)
<br>Dokumen ini mengharapkan kontribusi anda (dokumentasi, terjemahan, pelaporan, saran, pengkodean).

Akhir-akhir ini, berhubung dari bug projek Chromium seperti yang disebutkan di https://bugs.chromium.org/p/chromium/issues/detail?id=1177214, <br>versi 3Q20 mempunyai kendala kegagalan login Google.
<br>
<br>
Kami akan memperbaiki isu tersebut di versi 1Q21 Wayne OS.
